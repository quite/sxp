package main

import (
	"errors"
	"fmt"
	"io/fs"
	"net"
	"os"
	"path/filepath"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
)

type connection struct {
	sshConn  *ssh.Client
	sftpConn *sftp.Client
}

func connect(hkCallback ssh.HostKeyCallback, hostPort string, remoteUser string) (*connection, error) {
	fmt.Fprintf(os.Stdout, "Connecting to %s as %s\n", hostPort, remoteUser)

	socket := os.Getenv("SSH_AUTH_SOCK")
	agentConn, err := net.Dial("unix", socket)
	if err != nil {
		return nil, fmt.Errorf("Dial SSH_AUTH_SOCK failed: %w", err)
	}

	sshConn, err := ssh.Dial("tcp", hostPort,
		&ssh.ClientConfig{
			User: remoteUser,
			Auth: []ssh.AuthMethod{
				ssh.PublicKeysCallback(agent.NewClient(agentConn).Signers),
			},
			// Based on golang.org/x/crypto/ssh/common.go but ordered
			// like KEX_DEFAULT_PK_ALG in
			// openssh-portable/myproposal.h (to avoid HostKey
			// mismatch), and with RSA/SHA1 and DSA/SHA1 removed.
			HostKeyAlgorithms: []string{
				ssh.CertAlgoED25519v01,
				ssh.CertAlgoECDSA256v01, ssh.CertAlgoECDSA384v01, ssh.CertAlgoECDSA521v01,
				ssh.CertAlgoRSASHA512v01, ssh.CertAlgoRSASHA256v01,
				ssh.KeyAlgoED25519,
				ssh.KeyAlgoECDSA256, ssh.KeyAlgoECDSA384, ssh.KeyAlgoECDSA521,
				ssh.KeyAlgoRSASHA256, ssh.KeyAlgoRSASHA512,
			},
			HostKeyCallback: hkCallback,
		})
	if err != nil {
		return nil, fmt.Errorf("Dial failed: %w", err)
	}

	sftpConn, err := sftp.NewClient(sshConn,
		sftp.UseConcurrentReads(true),
		sftp.UseConcurrentWrites(true),
	)
	if err != nil {
		_ = sshConn.Close()
		return nil, fmt.Errorf("sftp.NewClient failed: %w", err)
	}

	return &connection{
		sshConn:  sshConn,
		sftpConn: sftpConn,
	}, nil
}

func (c *connection) close() {
	if c.sftpConn != nil {
		if err := c.sftpConn.Close(); err != nil {
			fmt.Fprintf(os.Stderr, "sftp Close failed: %s\n", err)
		}
	}
	if c.sshConn != nil {
		if err := c.sshConn.Close(); err != nil {
			fmt.Fprintf(os.Stderr, "ssh Close failed: %s\n", err)
		}
	}
}

func copyFiles(srcs []string, conn *connection, destDir string) error {
	if destDir != "" {
		fi, err := conn.sftpConn.Lstat(destDir)
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				return fmt.Errorf("destination directory does not exist")
			}
			return fmt.Errorf("remote stat %s failed: %w", destDir, err)
		}
		if !fi.IsDir() {
			return fmt.Errorf("destination is not a directory")
		}
	}

	for _, src := range srcs {
		fi, err := os.Lstat(src)
		if err != nil {
			if errors.Is(err, os.ErrNotExist) {
				fmt.Fprintf(os.Stderr, "%s: does not exist\n", src)
			} else {
				fmt.Fprintf(os.Stderr, "%s: stat failed: %s\n", src, err)
			}
			continue
		}

		switch {
		case fi.Mode().IsDir():
			if !quiet {
				fmt.Fprintf(os.Stdout, "%s: skipped directory\n", src)
			}
		case (fi.Mode() & fs.ModeSymlink) != 0:
			target, err := os.Readlink(src)
			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: Readlink failed: %s\n", src, err)
			} else {
				linkName := filepath.Join(destDir, filepath.Base(src))
				if err := createSymlink(src, conn, target, linkName); err != nil {
					fmt.Fprintf(os.Stderr, "%s: symlink failed: %s\n", src, err)
				}
			}
		case !fi.Mode().IsRegular():
			if !quiet {
				fmt.Fprintf(os.Stdout, "%s: skipped non-regular file\n", src)
			}
		default:
			dest := filepath.Join(destDir, filepath.Base(src))
			if err := copyFile(src, conn, dest); err != nil {
				fmt.Fprintf(os.Stderr, "%s: copyFile failed: %s\n", src, err)
			}
		}
	}

	return nil
}

func copyFile(srcFileName string, conn *connection, destFileName string) error {
	if !overwrite {
		if _, err := conn.sftpConn.Lstat(destFileName); err != nil {
			if !errors.Is(err, os.ErrNotExist) {
				return fmt.Errorf("remote stat %s failed: %w", destFileName, err)
			}
			// here we are good, file does not exist!
		} else {
			if !quiet {
				fmt.Fprintf(os.Stdout, "%s: skipped, not overwriting in %s/\n", srcFileName, filepath.Dir(destFileName))
			}
			return nil
		}
	}

	fmt.Fprintf(os.Stdout, "%s -> %s\n", srcFileName, destFileName)

	src, err := os.Open(srcFileName)
	if err != nil {
		return err
	}
	defer src.Close()

	dest, err := conn.sftpConn.Create(destFileName)
	if err != nil {
		return fmt.Errorf("remote create %s failed: %w", destFileName, err)
	}

	_, err = src.WriteTo(dest)
	_ = dest.Close()
	if err != nil {
		_ = conn.sftpConn.Remove(destFileName)
		return fmt.Errorf("WriteTo failed: %w", err)
	}

	return nil
}

func createSymlink(srcFileName string, conn *connection, target string, linkName string) error {
	if _, err := conn.sftpConn.Lstat(linkName); err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			return fmt.Errorf("remote stat %s failed: %w", linkName, err)
		}
		// here we are good, file does not exist!
	} else {
		if !overwrite {
			if !quiet {
				fmt.Fprintf(os.Stdout, "%s: skipped, not overwriting in %s/\n", srcFileName, filepath.Dir(linkName))
			}
			return nil
		}
		if err = conn.sftpConn.Remove(linkName); err != nil {
			return fmt.Errorf("remote remove symlink %s failed: %w", linkName, err)
		}
	}

	fmt.Fprintf(os.Stdout, "%s -> %s\n", srcFileName, linkName)

	if err := conn.sftpConn.Symlink(target, linkName); err != nil {
		return fmt.Errorf("remote symlink failed: %w", err)
	}

	return nil
}
