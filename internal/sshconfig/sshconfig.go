package sshconfig

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"
)

type SSHConfig struct {
	configHosts []ConfigHost
}

type ConfigHost struct {
	patterns []string
	hostname string
	port     uint
	user     string
}

// Does not handle:
// - keyword[whitespace]=arg with spaces
// - keyword "arg with spaces"
// - keyword arg #comment

var (
	includeBaseDir string
	ourHomeDir     string
)

func New(configFileName string, homeDir string) (*SSHConfig, error) {
	ourHomeDir = homeDir
	// This is what ssh(1) uses as base for all Include files, also
	// when -F is used
	includeBaseDir = filepath.Join(homeDir, ".ssh")

	configData := &bytes.Buffer{}
	if err := inlineIncludes(configData, configFileName); err != nil {
		return nil, err
	}

	configHosts, err := parse(configData)
	if err != nil {
		return nil, err
	}

	return &SSHConfig{configHosts: configHosts}, nil
}

func (c *SSHConfig) Get(host string) (string, uint, string) {
	var hostname string
	var port uint
	var user string

	for _, configHost := range c.configHosts {
		var matchedNegatedPattern bool
		for _, pattern := range configHost.patterns {
			if pattern[0] != '!' {
				continue
			}
			pattern = pattern[1:]
			// we have already checked the glob patterns
			if matched, _ := filepath.Match(pattern, host); !matched {
				continue
			}
			matchedNegatedPattern = true
		}
		if matchedNegatedPattern {
			continue
		}

		for _, pattern := range configHost.patterns {
			if pattern[0] == '!' {
				continue
			}
			// we have already checked the glob patterns
			if matched, _ := filepath.Match(pattern, host); !matched {
				continue
			}

			// only caring about the first instance of each keyword
			if hostname == "" {
				hostname = configHost.hostname
			}
			if port == 0 {
				port = configHost.port
			}
			if user == "" {
				user = configHost.user
			}
		}
	}

	return hostname, port, user
}

func inlineIncludes(w io.Writer, configFileName string) error {
	f, err := os.Open(configFileName)
	if err != nil {
		return err
	}
	defer f.Close()

	// default split func is ScanLines, max line length 64k
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		line := sc.Text()
		keyword, args := splitLine(line)
		if keyword == "" {
			continue
		}

		if keyword == "include" {
			if len(args) == 0 {
				return fmt.Errorf("Include keyword with no arg: %s", line)
			}
			for _, includeFileName := range args {
				includeFileName = expandTilde(includeFileName)
				if !filepath.IsAbs(includeFileName) {
					includeFileName = filepath.Join(includeBaseDir, includeFileName)
				}
				// Globbing rids us of non-existing files
				matches, err := filepath.Glob(includeFileName)
				if err != nil {
					return fmt.Errorf("Include keyword has %w: %s\n", err, line)
				}
				for _, match := range matches {
					if err := inlineIncludes(w, match); err != nil {
						return err
					}
				}
			}
		} else {
			_, _ = w.Write(sc.Bytes())
			_, _ = w.Write([]byte("\n"))
		}
	}

	return nil
}

func expandTilde(s string) string {
	if !strings.HasPrefix(s, "~") {
		return s
	}

	if s == "~" {
		return ourHomeDir
	}

	if strings.HasPrefix(s, "~/") {
		return ourHomeDir + s[1:]
	}

	var otherUserName, path string
	if i := strings.Index(s, "/"); i >= 0 {
		otherUserName, path = s[1:i], s[i:]
	} else {
		otherUserName, path = s[1:], ""
	}

	otherUser, err := user.Lookup(otherUserName)
	if err != nil {
		return s
	}

	return otherUser.HomeDir + path
}

func parse(r io.Reader) ([]ConfigHost, error) {
	var inHost *ConfigHost
	inMatch := false

	// keeping a synthesized wildcard host first, used for keywords
	// that are found outside (before) any Host/Match block
	configHosts := []ConfigHost{{patterns: []string{"*"}}}
	firstEntry := &configHosts[0]

	// default split func is ScanLines, max line length 64k
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		keyword, args := splitLine(line)
		if keyword == "" {
			continue
		}

		switch keyword {
		case "host":
			if len(args) == 0 {
				return nil, fmt.Errorf("Host keyword with no arg: %s", line)
			}
			// check the glob patterns in advance
			for i := range args {
				pattern := args[i]
				if strings.HasPrefix(pattern, "!") {
					pattern = pattern[1:]
					if len(pattern) == 0 {
						return nil, fmt.Errorf("Host keyword with odd single '!': %s", line)
					}
				}
				if _, err := filepath.Match(args[i], "dummy"); err != nil {
					return nil, fmt.Errorf("Host keyword has %w: %s", err, line)
				}
			}
			if inHost != nil {
				configHosts = append(configHosts, *inHost)
			}
			inHost = &ConfigHost{patterns: args}
			inMatch = false
		case "match":
			// TODO not caring about missing/incorrect arg; keywords
			// found in Match block are just ignored later
			if inHost != nil {
				configHosts = append(configHosts, *inHost)
			}
			inMatch = true
			inHost = nil
		case "port":
			if len(args) != 1 {
				return nil, fmt.Errorf("Port keyword must have exactly 1 arg: %s", line)
			}
			port, err := strconv.Atoi(args[0])
			if err != nil || port < 1 || port > 65535 {
				return nil, fmt.Errorf("port is not valid: %s", line)
			}
			switch {
			case inMatch:
				fmt.Fprintf(os.Stderr, "sshconfig: Match block is not handled, line ignored: %s\n", line)
			case inHost != nil:
				if inHost.port == 0 {
					inHost.port = uint(port)
				}
			default:
				if firstEntry.port == 0 {
					firstEntry.port = uint(port)
				}
			}
		case "hostname":
			if len(args) != 1 {
				return nil, fmt.Errorf("Hostname keyword must have exactly 1 arg: %s", line)
			}
			switch {
			case inMatch:
				fmt.Fprintf(os.Stderr, "sshconfig: Match block is not handled, line ignored: %s\n", line)
			case inHost != nil:
				if inHost.hostname == "" {
					inHost.hostname = args[0]
				}
			default:
				if firstEntry.hostname == "" {
					firstEntry.hostname = args[0]
				}
			}
		case "user":
			if len(args) != 1 {
				return nil, fmt.Errorf("User keyword must have exactly 1 arg: %s", line)
			}
			switch {
			case inMatch:
				fmt.Fprintf(os.Stderr, "sshconfig: Match block is not handled, line ignored: %s\n", line)
			case inHost != nil:
				if inHost.user == "" {
					inHost.user = args[0]
				}
			default:
				if firstEntry.user == "" {
					firstEntry.user = args[0]
				}
			}
		case "include":
			return nil, fmt.Errorf("unexpected Include keyword: %s", line)
		}
	}

	if err := sc.Err(); err != nil {
		return nil, fmt.Errorf("scanner err: %w", err)
	}

	return configHosts, nil
}

func splitLine(line string) (string, []string) {
	var ss []string
	for _, s := range strings.Split(strings.Trim(line, "\t "), " ") {
		if s = strings.Trim(s, "\t "); s != "" {
			ss = append(ss, s)
		}
	}
	if len(ss) == 0 || strings.HasPrefix(ss[0], "#") {
		return "", []string{}
	}
	return strings.ToLower(ss[0]), ss[1:]
}
