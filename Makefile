.PHONY: sxp
sxp:
	go build

install:
	GOBIN=~/bin go install

.PHONY: test
test: sxp
	rm -rf test
	mkdir -p test/src test/dest
	echo data1 >test/src/file1
	echo data2 >test/src/file2
	mkfifo test/src/fifo1
	ln -sr test/src/file1 test/src/ln1
	ln -sr test/src/file1 test/src/ln2
	ln -sr test/src/nope test/src/broken
	mkdir test/src/dir1
	./sxp    ./test/src/* localhost:$$(pwd)/test/dest
	./sxp -f ./test/src/* localhost:$$(pwd)/test/dest
