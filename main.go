package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"strings"

	"codeberg.org/quite/sxp/internal/sshconfig"
	"golang.org/x/crypto/ssh/knownhosts"
)

var (
	overwrite bool
	quiet     bool
)

func main() {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	var port uint
	var config string

	portFlag := "P"
	portDesc := "Port for SSH on remote host"
	portDefault := uint(22)
	flag.UintVar(&port, portFlag, portDefault, portDesc)

	overwriteFlag := "f"
	overwriteDesc := "Overwrite existing files"
	overwriteDefault := false
	flag.BoolVar(&overwrite, overwriteFlag, overwriteDefault, overwriteDesc)

	configFlag := "F"
	configDesc := "Per-user SSH config file (can be 'none')"
	configDefault := filepath.Join(homeDir, ".ssh/config")
	flag.StringVar(&config, configFlag, configDefault, configDesc)

	quietFlag := "q"
	quietDesc := "Don't output warnings and information about skipped files etc (but still errors)"
	quietDefault := false
	flag.BoolVar(&quiet, quietFlag, quietDefault, quietDesc)

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `Usage:
sxp [options] SOURCE... [user@]remote.host.example:[DESTINATION]

Transfer files using the SFTP protocol over SSH.

Authentication is done through an ssh-agent pointed at by the
environment variable SSH_AUTH_SOCK. Host keys are checked against
~/.ssh/known_hosts which is populated by regular ssh/ssh-keyscan use.

By default, the SSH config file ~/.ssh/ssh_config is read and parsed
selectively. Only the Include/Hostname/Port/User keywords and Host
blocks are recognized. Notably, the Match block is ignored entirely,
as are some other lesser used features and symbols.

SOURCEs are copied to DESTINATION directory on the remote host. If
SOURCE is not a regular file it is skipped, but symbolic links are
recreated in DESTINATION (not dereferenced).

If DESTINATION is not given, it will typically be the remote user's
$HOME directory. If DESTINATION is given, it must exist.

Unless the -f flag is used, a file that already exists in the
DESTINATION directory is skipped and is not overwritten.

Options:
  -%s port  %s (default %d)
  -%s       %s (default %t)
  -%s file  %s
           (default %s)
  -%s       %s (default %t)
`,
			portFlag, portDesc, portDefault,
			overwriteFlag, overwriteDesc, overwriteDefault,
			configFlag, configDesc, configDefault,
			quietFlag, wrap(quietDesc, 70-11, 11), quietDefault)
	}

	flag.Parse()

	if port < 1 || port > 65535 {
		fmt.Fprintf(os.Stderr, "port %d is not valid\n", port)
		os.Exit(2)
	}

	args := flag.Args()

	if len(args) < 2 {
		flag.Usage()
		os.Exit(2)
	}

	srcs := args[:len(args)-1]
	hostname, remoteUser, destDir := parseDest(args[len(args)-1])

	hkCallback, err := knownhosts.New(filepath.Join(homeDir, ".ssh/known_hosts"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}

	if config != "none" {
		cfg, err := sshconfig.New(config, homeDir)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to read config: %s\n", err)
			os.Exit(1)
		}
		cfgHostname, cfgPort, cfgUser := cfg.Get(hostname)

		// always use hostname from config if present
		if cfgHostname != "" {
			hostname = cfgHostname
		}
		// user on cmdline takes precedence over user from config
		if remoteUser == "" && cfgUser != "" {
			remoteUser = cfgUser
		}
		// port on cmdline takes precedence over port from config
		var portFlag bool
		flag.Visit(func(f *flag.Flag) {
			if f.Name == "P" {
				portFlag = true
			}
		})
		if !portFlag && cfgPort > 0 {
			port = cfgPort
		}
	}

	if remoteUser == "" {
		u, err := user.Current()
		if err != nil {
			fmt.Fprintf(os.Stderr, "user.Current failed: %s\n", err)
			os.Exit(1)
		}
		remoteUser = u.Username
	}

	conn, err := connect(hkCallback, net.JoinHostPort(hostname, strconv.Itoa(int(port))), remoteUser)
	if err != nil {
		fmt.Fprintf(os.Stderr, "connect failed: %s\n", err)
		os.Exit(1)
	}

	if err := copyFiles(srcs, conn, destDir); err != nil {
		fmt.Fprintf(os.Stderr, "copyFiles failed: %s\n", err)
		os.Exit(1)
	}

	conn.close()
}

func parseDest(dest string) (string, string, string) {
	var user string
	if strings.Contains(dest, "@") {
		ss := strings.SplitN(dest, "@", 2)
		user = ss[0]
		dest = ss[1]
	}

	var host string
	var destDir string
	if !strings.Contains(dest, ":") {
		fmt.Fprintf(os.Stderr, "Expected a colon (:) in destination\n")
		os.Exit(2)
	}
	ss := strings.SplitN(dest, ":", 2)
	host = ss[0]
	if len(ss) > 1 {
		destDir = ss[1]
	}

	return host, user, destDir
}

func wrap(s string, cols int, indent int) string {
	words := strings.Fields(strings.TrimSpace(s))
	if len(words) == 0 {
		return s
	}
	out := words[0]
	left := cols - len(out)
	for _, w := range words[1:] {
		if (1 + len(w)) > left {
			out += "\n" + strings.Repeat(" ", indent) + w
			left = cols - len(w)
			continue
		}
		out += " " + w
		left -= (1 + len(w))
	}
	return out
}
