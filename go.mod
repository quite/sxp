module codeberg.org/quite/sxp

go 1.22

require (
	github.com/pkg/sftp v1.13.7
	golang.org/x/crypto v0.31.0
)

require (
	github.com/kr/fs v0.1.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
)
