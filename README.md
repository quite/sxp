A small sftp tool that can skip files that already exist on the
destination, which neither sftp(1) nor scp(1) can. Otherwise very
limited feature set. Things may be added as I need them.
